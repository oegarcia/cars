<?php

use Illuminate\Database\Seeder;

class OwnerTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      $table='owners';
        $records = [
            [
                'dni' => '111',              
                'name' => 'pedro',
                'surname' => 'perez',
            ],
            [
                'dni' => '222',              
                'name' => 'juan',
                'surname' => 'ramirez',
            ],
            [
                'dni' => '333',              
                'name' => 'maría',
                'surname' => 'rodriguez',
            ],
        ];
        DB::table($table)->delete();
        foreach ($records as $record) {
            DB::table($table)->insert($record);
        }
    }

}
