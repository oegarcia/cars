<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      $table='statuses';
        $records = [
            [
                'id' => 1,                
                'name' => 'en venta',
            ],
            [
                'id' => 2,                
                'name' => 'en alquiler',
            ],
            [
                'id' => 3,                
                'name' => 'para chatarizar',
            ],
        ];
        DB::table($table)->delete();
        foreach ($records as $record) {
            DB::table($table)->insert($record);
        }
    }

}
