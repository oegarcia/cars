@extends('layout')
@section('content')
<h1>Incluir carro</h1>
        <form action="{{route('cars.store')}}" method="post">
                {{ csrf_field() }}
            <label>Propietario</label>
            <select name="owner_id">
                <option value="0" selected disabled>selecciona un dueño</option>
                @foreach($owners as $owner)
                <option value="{{$owner->id}}">{{$owner->dni.'-'.$owner->name}}</option>
                @endforeach
            </select><br>
            <label>Placa</label>
            <input type="text" name="placa" required><br>
            <label>Color</label>
            <input type="text" name="color" required><br>
            <label>Fecha de Compra</label>
            <input type="date" name="fecha_compra" required><br>
            <button type="submit">guardar</button>
        </form>
@endsection