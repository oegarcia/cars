

            @extends('layout')
            @section('content')

        <h1>Carros</h1>
        <a href="{{route('cars.create')}}">incluir carro</a>
        <table class="table table-striped">
        <tr>
            
               <th><strong> placa</strong></th>
               <th><strong> color</strong></th>
               <th><strong> fecha de compra</strong></th>
               <th><strong> propietario</strong></th>
               <th><strong> status</strong></th>


            @foreach ($cars as $car)                
<tr>
                   <td> {{ $car->placa }} </td>
                   <td> {{ $car->color }} </td>
                   <td> {{ $car->fecha_compra }} </td>
                   <td> {{ $car->owner->name }} </td>
                   <td> {{ $car->status->name }} </td>
                   <td><a href="{{route('cars.edit',$car->id)}}">modificar</a></td>
                   <td>
                    <form id="delete-form" action="{{ route('cars.destroy',$car->id) }}" method="POST" >
                            {{ method_field('DELETE') }} 
                            {{ csrf_field() }}
                            <button type="submit">Eliminar</button>
                          </form>
                </td>
</tr>

            @endforeach
        </tr>
        </table
   @endsection