@extends('layout')
@section('content')
<h1>Modificar carro</h1>
        <form action="{{route('cars.update',$car->id)}}" method="post">
                {{ method_field('PATCH') }}  
                {{ csrf_field() }}
            <label>Propietario</label>
            <select name="owner_id">
                
                @foreach($owners as $owner)
                @if ($owner->id==$car->owner->id)
                <option value="{{$owner->id}}" selected>{{$owner->dni.'-'.$owner->name}}</option>
                @else
                <option value="{{$owner->id}}">{{$owner->dni.'-'.$owner->name}}</option>
                @endif
                @endforeach
            </select><br>
            <label>Placa</label>
            <input type="text" name="placa" required value="{{$car->placa}}"><br>
            <label>Color</label>
            <input type="text" name="color" required value="{{$car->color}}"><br>
            <label>Fecha de Compra</label>
            <input type="date" name="fecha_compra" required value="{{$car->fecha_compra}}"><br>
            <label>Propietario</label>
            <select name="status_id">
                
                @foreach($status as $statu)
                @if ($statu->id==$car->status_id)
                <option value="{{$statu->id}}" selected>{{$statu->name}}</option>
                @else
                <option value="{{$statu->id}}">{{$statu->name}}</option>
                @endif
                @endforeach
            </select><br>
            <button type="submit">guardar</button>
        </form>
@endsection