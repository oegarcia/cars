<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public $timestamps = false;
    protected $fillable = [        
        'placa',
        'color',
        'fecha_compra',
        'owner_id',
        'status_id'
    ];
    public function owner() {
        return $this->belongsTo('\App\Owner');
      }
      public function status() {
        return $this->belongsTo('\App\Status');
      }
}
